package com.guernik.frauddetection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsNot.not;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class FraudDetectorTest {

  private double fraudThreshold = 20.0;
  private Duration period = Duration.ofHours(24);
  private LocalDateTime now = LocalDateTime.now();
  private FraudDetector detector = new FraudDetector(period, fraudThreshold);

  @Test
  void receiveNonFraudCard() {
    detector.receive(new CardTransaction("1", now, fraudThreshold / 2));
    assertEquals(detector.getFraudCards().size(), 0);
  }

  @Test
  void receiveFraudCard() {
    detector.receive(new CardTransaction("1", now, fraudThreshold / 2));
    detector.receive(new CardTransaction("1", now, fraudThreshold / 2));
    detector.receive(new CardTransaction("1", now, fraudThreshold / 2));
    assertThat(detector.getFraudCards(), hasItem("1"));
  }

  @Test
  void onlyFraudCardsAreRegistered() {
    detector.receive(new CardTransaction("1", now, fraudThreshold * 2));
    detector.receive(new CardTransaction("2", now, fraudThreshold / 2));
    assertThat(detector.getFraudCards(), hasItem("1"));
    assertThat(detector.getFraudCards(), not(hasItem("2")));
  }
}