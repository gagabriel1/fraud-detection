package com.guernik.frauddetection;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class CardTransactionParserTest {

  private final CardTransactionParser cardTransactionParser = new CardTransactionParser();

  @Test
  void fromString() {
    CardTransaction transaction =
        cardTransactionParser.fromString("10d7ce2f43e35fa57d1bb, 2014-04-29T13:15:54, 10.00");
    assertEquals(transaction.getHashedCardNumber(), "10d7ce2f43e35fa57d1bb");
    assertEquals(transaction.getDateTime(), LocalDateTime.of(2014, 4, 29, 13, 15, 54));
    assertEquals(transaction.getAmount(), 10.0);
  }

  @ParameterizedTest
  @MethodSource
  void invalidParams(String transaction) {
    assertThrows(IllegalArgumentException.class,
        () -> cardTransactionParser.fromString(transaction));
  }

  @SuppressWarnings("unused")
  static Stream<String> invalidParams() {
    return Stream.of(null,
        "",
        "123,2014-04-29T13:15:54",
        "123,2014-04-29T13:15:54, ",
        "123, ,10.0",
        " ,2014-04-29T13:15:54,10.0",
        "123,2014-04-29T13:15:54,10.0,1.0",
        "123,2014-04-29 13:15:54,10.0",
        "123,2014-04-29T13:15:54,abd123");
  }

  @Test
  void fromStrings() {
    List<CardTransaction> transactions = cardTransactionParser.fromStrings(
        asList("1, 2001-01-01T01:01:01, 1.00", "2, 2002-02-02T02:02:02, 2.00"));

    assertEquals(transactions.get(0).getHashedCardNumber(), "1");
    assertEquals(transactions.get(0).getDateTime(), LocalDateTime.of(2001, 1, 1, 1, 1, 1));
    assertEquals(transactions.get(0).getAmount(), 1.0);

    assertEquals(transactions.get(1).getHashedCardNumber(), "2");
    assertEquals(transactions.get(1).getDateTime(), LocalDateTime.of(2002, 2, 2, 2, 2, 2));
    assertEquals(transactions.get(1).getAmount(), 2.0);
  }
}