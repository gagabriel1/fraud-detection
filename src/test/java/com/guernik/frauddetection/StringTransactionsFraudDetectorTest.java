package com.guernik.frauddetection;

import static java.time.Duration.ofHours;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsNot.not;

import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;

class StringTransactionsFraudDetectorTest {

  @Test
  void getFraudulentCardsForEmptyTransactions() {
    Set<String> fraudCards = StringTransactionsFraudDetector.getFraudCards(emptyList(),
        new FraudDetector(ofHours(24), 10.0));
    assertThat(fraudCards, is(empty()));
  }

  @Test
  void getFraudulentCardsForNonFraudCards() {
    List<String> transactions = asList("1, 2018-01-01T01:00:00, 9.0",
        "1, 2018-01-01T02:00:00, 0.9",
        "2, 2018-01-01T03:00:00, 9.9");
    Set<String> fraudCards = StringTransactionsFraudDetector.getFraudCards(transactions,
        new FraudDetector(ofHours(24), 10.0));
    assertThat(fraudCards, is(empty()));
  }

  @Test
  void getFraudulentCardsForFraudCard() {
    List<String> transactions = asList("1, 2018-01-01T01:00:00, 9.0",
        "1, 2018-01-01T02:00:00, 1.1",
        "2, 2018-01-01T03:00:00, 9.9");
    Set<String> fraudCards = StringTransactionsFraudDetector.getFraudCards(transactions,
        new FraudDetector(ofHours(24), 10.0));
    assertThat(fraudCards, hasItem("1"));
    assertThat(fraudCards, not(hasItem("2")));
  }
}