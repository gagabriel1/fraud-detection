package com.guernik.frauddetection;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class SlidingWindowTest {

  private final Duration period = Duration.ofHours(24);
  private final LocalDateTime now = LocalDateTime.now();

  @Test
  void receiveFirstTransaction() {
    SlidingWindow window = new SlidingWindow(period);
    window.receive(new CardTransaction("1", now, 10.0));
    // if any of the double comparisons fail due to precision,
    // we could use Assertions.assertEquals(double, double, double)
    assertEquals(window.getCurrentTotal(), 10.0);
  }

  @Test
  void receiveTransactionInWindow() {
    SlidingWindow window = new SlidingWindow(period);
    window.receive(new CardTransaction("1", now, 10.0));
    window.receive(new CardTransaction("1", now.plus(period.dividedBy(2)), 5.0));
    assertEquals(window.getCurrentTotal(), 15.0);
  }

  @Test
  void removeOldTransactionLeavingOnlyOne() {
    SlidingWindow window = new SlidingWindow(period);
    window.receive(new CardTransaction("1", now, 10.0));
    window.receive(new CardTransaction("1", now.plus(period).plusMinutes(1), 5.0));
    assertEquals(window.getCurrentTotal(), 5.0);
  }

  @Test
  void removeManyOldTransactions() {
    SlidingWindow window = new SlidingWindow(period);
    window.receive(new CardTransaction("1", now, 10.0));
    window.receive(new CardTransaction("1", now, 10.0));
    window.receive(new CardTransaction("1", now, 10.0));
    window.receive(new CardTransaction("1", now.plus(period.dividedBy(2)), 10.0));
    window.receive(new CardTransaction("1", now.plus(period).plusMinutes(1), 5.0));
    assertEquals(window.getCurrentTotal(), 15.0);
  }
}