package com.guernik.frauddetection;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Keeps track of the total amount of the transactions for a given period.
 *
 * It does not check that the transactions belong to the same card. So it could potentially be
 * used to track transactions for a single or multiple cards. If transactions for a single card
 * are to be tracked, then the client is responsible for providing transactions for only this card.
 *
 * Instances of this class are not thread safe.
 */
public class SlidingWindow {

  private Queue<CardTransaction> transactions = new LinkedList<>();
  private double currentTotal = 0;
  private final Duration period;

  public SlidingWindow(final Duration period) {
    this.period = period;
  }

  public void receive(CardTransaction transaction) {
    LocalDateTime cutOffTime = transaction.getDateTime().minus(period);
    removeOlderThan(cutOffTime);
    addOne(transaction);
  }

  /**
   * Adds an element without removing old ones.
   */
  private void addOne(CardTransaction transaction) {
    currentTotal += transaction.getAmount();
    transactions.add(transaction);
  }

  private void removeOlderThan(LocalDateTime cutOffTime) {
    while (transactions.peek() != null && transactions.peek().getDateTime().isBefore(
        cutOffTime)) {
      removeOne();
    }
  }

  /**
   * Must only be called if there is an element.
   */
  private void removeOne() {
    CardTransaction removed = transactions.poll();
    currentTotal -= removed.getAmount();
  }

  public double getCurrentTotal() {
    return currentTotal;
  }
}
