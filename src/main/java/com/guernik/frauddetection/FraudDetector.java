package com.guernik.frauddetection;

import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Looks for fraud cards given a list of transactions. A fraud card is one that has accumulated
 * transactions for more than the given threshold in any given period.
 *
 * It does not detect duplicate transactions.
 * It cannot be reset, so the same instance should not be used to process lists of transactions
 * that should be independent.
 *
 * Instances of this class are not thread safe.
 */
public class FraudDetector {

  private Map<String, SlidingWindow> slidingWindows = new HashMap<>();
  private final Duration period;
  private final double fraudThreshold;
  private Set<String> fraudCards = new HashSet<>();

  public FraudDetector(Duration period, double fraudThreshold) {
    this.period = period;
    this.fraudThreshold = fraudThreshold;
  }

  public void receive(CardTransaction transaction) {
    SlidingWindow window = getSlidingWindow(transaction);
    window.receive(transaction);
    checkFraud(transaction.getHashedCardNumber(), window);
  }

  public Set<String> getFraudCards() {
    return fraudCards;
  }

  private void checkFraud(String hashedCardNumber, SlidingWindow window) {
    if (window.getCurrentTotal() > fraudThreshold) {
      fraudCards.add(hashedCardNumber);
    }
  }

  private SlidingWindow getSlidingWindow(final CardTransaction transaction) {
    SlidingWindow window;
    if (slidingWindows.containsKey(transaction.getHashedCardNumber())) {
      window = slidingWindows.get(transaction.getHashedCardNumber());
    } else {
      window = new SlidingWindow(period);
      slidingWindows.put(transaction.getHashedCardNumber(), window);
    }
    return window;
  }
}
