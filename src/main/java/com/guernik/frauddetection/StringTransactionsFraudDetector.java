package com.guernik.frauddetection;

import java.util.List;
import java.util.Set;

/**
 * Finds fraud cards given a list of transactions.
 *
 * The transactions are given in a list of strings and are parsed by a
 * {@link CardTransactionParser}. The actual fraud detection happens in the {@link FraudDetector},
 * so same restrictions as in this one applies.
 */
public class StringTransactionsFraudDetector {

  private final FraudDetector detector;
  private final CardTransactionParser parser;

  private StringTransactionsFraudDetector(FraudDetector detector) {
    this.detector = detector;
    this.parser = new CardTransactionParser();
  }

  public static Set<String> getFraudCards(List<String> transactions, FraudDetector detector) {
    return new StringTransactionsFraudDetector(detector).getFraudCards(transactions);
  }

  private Set<String> getFraudCards(List<String> transactions) {
    List<CardTransaction> cardTransactions = parser.fromStrings(transactions);
    cardTransactions.forEach(detector::receive);
    return detector.getFraudCards();
  }
}
