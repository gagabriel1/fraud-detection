package com.guernik.frauddetection;

import java.time.LocalDateTime;

public class CardTransaction {

  private final String hashedCardNumber;
  private final LocalDateTime dateTime;
  // it would be recommended to use BigDecimal to deal with money, to avoid loss of precision.
  // as precision is not that critical here, double should be fine.
  // it could become more important if we process a lot of transactions.
  private final double amount;

  public CardTransaction(final String hashedCardNumber, final LocalDateTime dateTime,
                         final double amount) {
    this.hashedCardNumber = hashedCardNumber;
    this.dateTime = dateTime;
    this.amount = amount;
  }

  public String getHashedCardNumber() {
    return hashedCardNumber;
  }

  public LocalDateTime getDateTime() {
    return dateTime;
  }

  public double getAmount() {
    return amount;
  }
}
