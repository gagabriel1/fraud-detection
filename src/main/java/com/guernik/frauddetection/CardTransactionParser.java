package com.guernik.frauddetection;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CardTransactionParser {

  /**
   * Converts an iterable of transactions on a string form to a list {@link CardTransaction}.
   *
   * @return the parsed {@link CardTransaction} list
   *
   * @throws IllegalArgumentException if any of the strings is invalid
   * @see CardTransactionParser#fromString(String) fromString
   */
  public List<CardTransaction> fromStrings(Iterable<String> transactionStrings) {
    return StreamSupport.stream(transactionStrings.spliterator(), false)
        .map(this::fromString)
        .collect(Collectors.toList());
  }

  /**
   * Converts a string to a {@link CardTransaction}.
   *
   * The given string must have 3 elements separated by commas:
   * <ul>
   * <li>the first must be a string representing the hash of a card number</li>
   * <li>the second must be the transaction local date and time in the format
   * yyyy-MM-ddTHH:mm:ss, such as '2014-04-29T13:15:54'</li>
   * <li>the third must be the amount as a double precision number, such as 10.0.</li>
   * </ul>
   *
   * @return the parsed {@link CardTransaction}
   *
   * @throws IllegalArgumentException if the strings is null, empty or invalid
   */
  public CardTransaction fromString(String transactionString) {
    String[] transactionElements = toStringTransactionElements(transactionString);
    String cardNumberHash = toHashedCardNumber(transactionElements[0]);
    LocalDateTime dateTime = toLocalDateTime(transactionElements[1]);
    double amount = toAmount(transactionElements[2]);
    return new CardTransaction(cardNumberHash, dateTime, amount);
  }

  private String[] toStringTransactionElements(String transactionString) {
    if (Objects.isNull(transactionString) || transactionString.trim().isEmpty()) {
      throw new IllegalArgumentException("Transaction string is null or empty");
    }
    String[] transactionElements = transactionString.split(",");
    if (transactionElements.length != 3) {
      throw new IllegalArgumentException("Transaction string is missing elements, 3 are expected");
    }
    return transactionElements;
  }

  private String toHashedCardNumber(String hashedCardNumberString) {
    String cardNumberHash = hashedCardNumberString.trim();
    if (cardNumberHash.isEmpty()) {
      throw new IllegalArgumentException("Card number hash cannot be empty");
    }
    return cardNumberHash;
  }

  private LocalDateTime toLocalDateTime(String dateTimeString) {
    LocalDateTime dateTime;
    try {
      dateTime = LocalDateTime.parse(dateTimeString.trim());
    } catch (DateTimeParseException e) {
      throw new IllegalArgumentException("Date time has invalid format", e);
    }
    return dateTime;
  }

  private double toAmount(String amountString) {
    double amount;
    try {
      amount = Double.parseDouble(amountString.trim());
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("Amount format is invalid", e);
    }
    return amount;
  }
}
